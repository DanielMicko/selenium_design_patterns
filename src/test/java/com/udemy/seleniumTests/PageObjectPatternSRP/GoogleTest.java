package com.udemy.seleniumTests.PageObjectPatternSRP;

import com.udemy.seleniumDesign.PageObjectPatternSRP.mainPO.GoogleMainPage;
import com.udemy.seleniumDesign.PageObjectPatternSRP.resultPO.GoogleResultPage;
import com.udemy.seleniumTests.BaseTest;
import org.junit.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GoogleTest extends BaseTest {

    private GoogleMainPage googleMainPage;
    private GoogleResultPage googleResultPage;

    @BeforeTest
    public void setUpPages(){
        this.googleMainPage = new GoogleMainPage(driver);
        this.googleResultPage = new GoogleResultPage(driver);
    }

    //Page Object pattern následuje Single responsibility principle
    //To znamená, že stránku si rozdělíme do snippetů...každý snippet je poté jedna classa.
    //Všechny classy snippetů na dané stránce poté dáme dohromady v Page Object classe
    //Všechny vytvořené Page Object classy pak zavoláme na test classe a použitím jejich metod vytvoříme test


    @Test(dataProvider = "getData")
    public void googleWorkFlow(String keyword, int index){

        googleMainPage.goTo(); //launching website

        googleMainPage.acceptModal(); //accepting cookies iframe
        Assert.assertTrue(googleMainPage.getSearchWidget().isDisplayed()); //is main page displayed

        googleMainPage.getSearchWidget().enter(keyword); //enter keywords
        Assert.assertTrue(googleMainPage.getSearchSuggestion().isDisplayed()); //are suggestions display

        googleMainPage.getSearchSuggestion().clickSuggestionByIndex(index); //select suggestion by index
        Assert.assertTrue(googleResultPage.getNavigationBar().isDisplayed()); //are we on search page

        googleResultPage.getSearchWidget().enter(keyword); //enter keywords
        Assert.assertTrue(googleResultPage.getSearchSuggestion().isDisplayed()); //are suggestions displayed

        googleResultPage.getSearchSuggestion().clickSuggestionByIndex(index); //select by index
        Assert.assertTrue(googleResultPage.getNavigationBar().isDisplayed()); //are we on searchpage

        googleResultPage.getNavigationBar().goToVideos(); //click on videos tab
        Assert.assertTrue(googleResultPage.getResultStat().isDisplayed()); //are the results displayed
        System.out.println(
                googleResultPage.getResultStat().getStat()
        ); //print result stat
        googleResultPage.getNavigationBar().goToNews(); //click on news tab
        Assert.assertTrue(googleResultPage.getResultStat().isDisplayed()); //are the results displayed
        System.out.println(
                googleResultPage.getResultStat().getStat()
        ); //print result stat
    }

    @DataProvider
    public Object[][] getData(){
        return new Object[][]{
                {"selenium",3}
                //{"docker", 2} nefunkční kůvli acceptModal iframu --- solution: vyřešit, aby se acceptModal spouštěl pouze jednou.
        };
    }


}
