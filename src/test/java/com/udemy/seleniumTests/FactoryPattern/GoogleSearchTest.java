package com.udemy.seleniumTests.FactoryPattern;

import com.udemy.seleniumDesign.FactoryPattern.GoogleFactory;
import com.udemy.seleniumDesign.FactoryPattern.GoogleSearchPage;
import com.udemy.seleniumTests.BaseTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GoogleSearchTest extends BaseTest {

    private GoogleSearchPage googleSearchPage;

    //Factory design pattern nám umožňuje vytvořit Factory constructor, kde můžeme vytvořit instance tříd (class)
    //Po vytvoření instancí tyto třídy můžeme mapovat a použít je postupně v našem testu pomocí data providera
    //Jednoduše vytvoříme map na factory classe, constructor factory metody si zavoláme v testu a pomocí proměnné "language" voláme jednotlivé classy
    //Language proměnné uchováváme v data provideru...díky mapu stačí vložit do language string (který reprezentuje danou classu)


    @Test(dataProvider = "getData")
    public void searchtest(String language, String keyword){

        this.googleSearchPage = GoogleFactory.get(language,this.driver);

        this.googleSearchPage.launchSite();
        this.googleSearchPage.acceptCookies();
        this.googleSearchPage.settings();
        this.googleSearchPage.selectLanguage();
        this.googleSearchPage.saveLanguage();
        this.googleSearchPage.search(keyword);
        int resultCount = this.googleSearchPage.getResultsCount();

        System.out.println("Result count is: "+resultCount);
    }

    @DataProvider
    public Object[][] getData(){
        return new Object[][]{
            {"ENG","selenium"},
            {"FR","design"},
            {"SA","docker"}
        };
    }
}
