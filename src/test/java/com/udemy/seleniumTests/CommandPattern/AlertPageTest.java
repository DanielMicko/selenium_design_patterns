package com.udemy.seleniumTests.CommandPattern;

import com.google.common.util.concurrent.Uninterruptibles;
import com.udemy.seleniumDesign.CommandPattern.ElementValidator;
import com.udemy.seleniumDesign.CommandPattern.HomePage;
import com.udemy.seleniumTests.BaseTest;
import net.bytebuddy.utility.RandomString;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

public class AlertPageTest{

    private HomePage homePage;
    private WebDriver driver;
    String s = RandomString.make(6);

    @BeforeTest
    public void setHomePage(){
        System.setProperty("webdriver.chrome.driver","D:\\SourceTree\\webdriver\\chromedriver.exe");
        this.driver = new ChromeDriver();
        this.driver.manage().window().maximize();
        this.homePage = new HomePage(driver);
    }

    //Díky command patternu můžeme přemístit business logiku na objekt, který provádí validace.
    //java syntax v testu právě pouští jednotlivé validace. Můžeme je spustit i paralelně.
    //Velmi užitečné, když máme hodně elementů, které potřebujeme validovat

    @Test
    public void homePageTest(){
        this.setHomePage();
        this.homePage.launchSite();
        System.out.println(s);

        //foor loop metoda pro validaci
        /*for (ElementValidator ev: this.homePage.getElementValidators()){
            boolean result = ev.validate();
            System.out.println(result);
            Assert.assertTrue(result);
        }*/

        //test sice neassertuje správně, ale tohle je cool - metoda parallel provádí všechny akce command patternu zaráz
        //Java 8 syntax metoda pro validaci
        this.homePage.getElementValidators()
                .stream()
                .parallel() //zde java vytvoří více vláken, které dělají validaci paralelně
                .map(ev -> ev.validate())
                .forEach(b -> Assert.assertTrue(b));
    }

    @AfterTest
    public void tearDown(){
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
        this.driver.close();
        this.driver.quit();
    }


}
