package com.udemy.seleniumTests.DecoratorPattern;


import com.udemy.seleniumDesign.DecoratorPattern.DashboardPage;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.function.Consumer;

public class Decorators {

    private static void shouldDisplay(List<WebElement> elements){
        elements.forEach(element -> Assert.assertTrue(element.isDisplayed()));
    }

    private static void shouldNotDisplay(List<WebElement> elements){
        elements.forEach(element -> Assert.assertFalse(element.isDisplayed()));
    }

    //komponenty jednotlivých rolí
    private static final Consumer<DashboardPage> adminCompPresent = (dashboardPage -> shouldDisplay(dashboardPage.getAdminComponents()));
    private static final Consumer<DashboardPage> adminCompNotPresent = (dashboardPage -> shouldNotDisplay(dashboardPage.getAdminComponents()));
    private static final Consumer<DashboardPage> suCompPresent = (dashboardPage -> shouldDisplay(dashboardPage.getSuperuserComponents()));
    private static final Consumer<DashboardPage> suCompNotPresent = (dashboardPage -> shouldNotDisplay(dashboardPage.getSuperuserComponents()));
    private static final Consumer<DashboardPage> guestCompPresent = (dashboardPage -> shouldDisplay(dashboardPage.getGuestComponents()));
    private static final Consumer<DashboardPage> guestCompNotPresent = (dashboardPage -> shouldNotDisplay(dashboardPage.getGuestComponents()));

    //role selection
    private static final Consumer<DashboardPage> adminSelection = (dashboardPage -> dashboardPage.selectRole("admin"));
    private static final Consumer<DashboardPage> suSelection = (dashboardPage -> dashboardPage.selectRole("superuser"));
    private static final Consumer<DashboardPage> guestSelection = (dashboardPage -> dashboardPage.selectRole("guest"));

    //user role pages
    public static final Consumer<DashboardPage> guestPage = guestSelection.andThen(guestCompPresent).andThen(suCompNotPresent).andThen(adminCompNotPresent);
    public static final Consumer<DashboardPage> suPage = suSelection.andThen(guestCompPresent).andThen(suCompPresent).andThen(adminCompNotPresent);
    public static final Consumer<DashboardPage> adminPage = adminSelection.andThen(guestCompPresent).andThen(suCompPresent).andThen(adminCompPresent);
}









