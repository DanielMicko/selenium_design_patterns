package com.udemy.seleniumTests.tryouts;


import com.udemy.seleniumDesign.tryouts.driverWait;
import com.udemy.seleniumDesign.tryouts.searchInput;
import com.udemy.seleniumDesign.tryouts.selectFromDropdown;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class sampleWorkflow {

    public WebDriver driver;
    String myText = "tree";

    searchInput newSearch = new searchInput(driver);
    selectFromDropdown dropdownSearch = new selectFromDropdown(driver);
    driverWait myDriverWait = new driverWait(driver);

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/Users/Daniel/IdeaProjects/SeleniumDesignPatterns/src/test/resources/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://google.com/");
    }

    @Test
    public void myGoogleSearch(){
        //switch to iFrame and back
        driver.switchTo().frame(0);
        myDriverWait.driverWaitVisible("//c-wiz[@class='SSPGKf fkimef']");
        driver.findElement(By.xpath("//span[text()='Souhlasím']")).click();
        driver.switchTo().defaultContent();

        //input my text and search it
        newSearch.searchGoogle("//input[@class='gLFyf gsfi']",myText);
        //myDriverWait.driverWaitVisible("//ul[@class='erkvQe']");

        //get keyword
        String listText = driver.findElement(By.xpath("//li[3]")).getText();

        //select 3rd result
        dropdownSearch.selectListElement("//ul[@class='erkvQe']", 2);

        //clear input
        driver.findElement(By.xpath("//input[@class='gLFyf gsfi']")).clear();

        //input keyword and search it
        newSearch.searchGoogle("//input[@class='gLFyf gsfi']",listText);
        dropdownSearch.selectListElement("//ul[@class='erkvQe']", 2);

        //click on "videos"
        driver.findElement(By.xpath("//a[text()='Videa']")).click();
        //myDriverWait.driverWaitVisible("//div[@id='result-stats']");

        //print results
        System.out.println(driver.findElement(By.xpath("//div[@id='result-stats']")).getText());
    }
}
