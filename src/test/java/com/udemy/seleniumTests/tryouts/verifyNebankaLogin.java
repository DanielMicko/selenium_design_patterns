package com.udemy.seleniumTests.tryouts;

import com.udemy.seleniumDesign.tryouts.loginPage;
import com.udemy.seleniumDesign.tryouts.loginPageNew;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

public class verifyNebankaLogin {

    public WebDriver driver;
    String email = "daniel.micko+2121@getfix.cz";
    String password = "Heslo123*";

    @Before
    public void setUp(){
        System.setProperty("webdriver.chrome.driver","/Users/Daniel/IdeaProjects/SeleniumDesignPatterns/src/test/resources/drivers/chromedriver");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://nebankaclient-staging.azurewebsites.net/");
    }

    @Test //přistupování na PageFactory bez class builderu
    public void verifyValidLogin(){
        loginPage login = new loginPage(driver);

        login.typeUserName(email);

        login.typePassword(password);

        login.clickOnLogin();
    }

    @Test //přistupování na PageFactory s class builderem dvěmi způsoby
    public void verifyValidLoginNew(){
        //zadefinování nové proměnné z vytvořené classy a zavolání funkce login
        loginPageNew login = new loginPageNew(driver);
        login.login_nebanka(email,password);
        //zadefinování nové proměnné z classy pomocí PageFactory a zavolání funkce login
        loginPageNew login_page_new = PageFactory.initElements(driver, loginPageNew.class);
        login_page_new.login_nebanka(email,password);


    }


}
