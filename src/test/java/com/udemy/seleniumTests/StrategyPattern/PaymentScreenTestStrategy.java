package com.udemy.seleniumTests.StrategyPattern;

import com.google.common.util.concurrent.Uninterruptibles;
import com.udemy.seleniumDesign.StrategyPattern.Main.PaymentScreen;
import com.udemy.seleniumDesign.StrategyPattern.PaymentMethod.CreditCard;
import com.udemy.seleniumDesign.StrategyPattern.PaymentMethod.NetBanking;
import com.udemy.seleniumDesign.StrategyPattern.PaymentMethod.PayPal;
import com.udemy.seleniumDesign.StrategyPattern.StrategyPatternPaymentOption;
import com.udemy.seleniumTests.BaseTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.collections.Maps;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class PaymentScreenTestStrategy extends BaseTest {

    private PaymentScreen paymentScreen;

    @BeforeTest
    public void setPaymentScreen(){
        this.paymentScreen = new PaymentScreen(this.driver);
    }

    //Strategy pattern modifikuje chování testu. Jak vidíme, v tomto testu vyplňujeme platební informace.
    //Způsobů plateb je několik - kartou, účtem, paypal. U všech jsou jiné inputy. Přes data provider můžeme
    //Implementujeme interface platebním metodám a poté si přes interface metodu voláme jednotlivé způsoby plateb.

    @Test(dataProvider = "getData")
    public void paymentTest(StrategyPatternPaymentOption paymentOption, Map<String, String> paymentDetails){
        this.paymentScreen.launchSite();
        this.paymentScreen.getUserInformation().enterDetails("Daniel","Hraniel","daniel.hraniel@ss.cc");
        this.paymentScreen.setPaymentOption(paymentOption);
        this.paymentScreen.getPaymentOption().enterPaymentInformation(paymentDetails);
        String order = this.paymentScreen.getOrder().placeOrder();

        System.out.println("Order number is: "+ order);
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
    }

    @DataProvider
    public Object[][]getData(){

        Map<String, String> cc = Maps.newHashMap();
        cc.put("cc","1111111111111");
        cc.put("year", "2020");
        cc.put("cvv", "111");

        Map<String, String> nb = Maps.newHashMap();
        nb.put("bank","BOFA");
        nb.put("accNum", "223445778");
        nb.put("pin", "1234");

        Map<String, String> pp = Maps.newHashMap();
        pp.put("username","já");
        pp.put("password", "1122");

        return new Object[][]{
                {new CreditCard(),cc},
                {new NetBanking(), nb},
                {new PayPal(), pp}
        };
    }

}
