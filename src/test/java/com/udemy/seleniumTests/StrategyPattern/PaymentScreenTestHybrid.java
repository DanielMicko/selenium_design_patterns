package com.udemy.seleniumTests.StrategyPattern;

import com.google.common.util.concurrent.Uninterruptibles;
import com.udemy.seleniumDesign.StrategyPattern.FactoryPatternPaymentOption;
import com.udemy.seleniumDesign.StrategyPattern.Main.PaymentScreen;
import com.udemy.seleniumTests.BaseTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.collections.Maps;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class PaymentScreenTestHybrid extends BaseTest {

    private PaymentScreen paymentScreen;

    @BeforeTest
    public void setPaymentScreen(){
        this.paymentScreen = new PaymentScreen(this.driver);
    }

    //Strategy Pattern můžeme kombinovat s factory patternem

    @Test(dataProvider = "getData")
    public void paymentTest(String option, Map<String, String> paymentDetails){
        this.paymentScreen.launchSite();
        this.paymentScreen.getUserInformation().enterDetails("Daniel","Hraniel","daniel.hraniel@ss.cc");
        this.paymentScreen.setPaymentOption(FactoryPatternPaymentOption.get(option));
        this.paymentScreen.getPaymentOption().enterPaymentInformation(paymentDetails);
        String order = this.paymentScreen.getOrder().placeOrder();

        System.out.println("Order number is: "+ order);
        Uninterruptibles.sleepUninterruptibly(3, TimeUnit.SECONDS);
    }

    @DataProvider
    public Object[][]getData(){

        Map<String, String> cc = Maps.newHashMap();
        cc.put("cc","1111111111111");
        cc.put("year", "2020");
        cc.put("cvv", "111");

        Map<String, String> nb = Maps.newHashMap();
        nb.put("bank","BOFA");
        nb.put("accNum", "223445778");
        nb.put("pin", "1234");

        Map<String, String> pp = Maps.newHashMap();
        pp.put("username","já");
        pp.put("password", "1122");

        return new Object[][]{
                {"CC",cc},
                {"NB", nb},
                {"PP", pp}
        };
    }

}
