package com.udemy.seleniumTests.ExecuteAroundMethodPattern;

import com.udemy.seleniumDesign.ExecuteAroundMethodPattern.MainPage;
import com.udemy.seleniumTests.BaseTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class FrameTest extends BaseTest {

    private MainPage mainPage;

    //Tento pattern je dobře použitelný při přepínání mezi soubory, databázemi, okny windows, framy atd.

    @BeforeTest
    public void setMainPage(){
        this.mainPage = new MainPage(driver);
    }

    @Test
    public void frameTest(){
        this.mainPage.goTo();

        this.mainPage.onFrameA(a -> {
            a.inputFn("FN");
            a.inputMess("MESS");
        });
        //this.mainPage.onFrameA(a -> a.inputMess("MESS"));
        this.mainPage.onFrameB(b -> b.inputLn("LNLN"));

        this.mainPage.onFrameC(c -> c.inputAdd("ADDDD"));
    }
}
