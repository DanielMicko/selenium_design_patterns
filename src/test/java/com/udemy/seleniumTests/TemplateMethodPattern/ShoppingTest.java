package com.udemy.seleniumTests.TemplateMethodPattern;

import com.udemy.seleniumDesign.TemplateMethodPattern.AmazonPageObject.AmazonShopping;
import com.udemy.seleniumDesign.TemplateMethodPattern.EbayShopping;
import com.udemy.seleniumDesign.TemplateMethodPattern.ShoppingTemplate;
import com.udemy.seleniumTests.BaseTest;
import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;

public class ShoppingTest extends BaseTest {

    //Template Method se používá při workflow, které mají stejné kroky, ale krok má rozdílné provedení (jiné elementy atd)
    //Podstata je, že algoritmus workflow (v abstraktní classe) je pořád stejný, jediné co se mění jsou způsoby, jak se kroky provádějí
    //Algoritmus pro nákup na amazonu i ebay byl stejný, Overridovaly se pouze metody z abstract classy podle potřeby

    @Test(dataProvider = "getData")
    public void shoppingTest(ShoppingTemplate shoppingTemplate){
        shoppingTemplate.shop();
    }

    @DataProvider
    public Object[] getData(){
        return new Object[]{
                new AmazonShopping(driver,"macbook"),
                new EbayShopping(driver, "macbook")
        };
    }
}
