package com.udemy.seleniumTests.DecoratorAssignment;

import com.udemy.seleniumDesign.DecoratorAssignment.PaymentScreen;
import org.testng.Assert;


import java.util.function.Consumer;

public class AssignmentDecorators {

    //insert credit card
    private static final Consumer<PaymentScreen> insertCcPass = (paymentScreen -> paymentScreen.insertCreditCard("4111111111111111","2023","123"));
    private static final Consumer<PaymentScreen> insertCcFail = (paymentScreen -> paymentScreen.insertCreditCard("4111111","023","13"));

    //insert code
    private static final Consumer<PaymentScreen> partialDiscount = (paymentScreen -> paymentScreen.insertCode("PARTIALUDEMY"));
    private static final Consumer<PaymentScreen> fullDiscount = (paymentScreen -> paymentScreen.insertCode("FREEUDEMY"));

    //buy product
    private static final Consumer<PaymentScreen> buy = (paymentScreen -> paymentScreen.buy());

    //Assertions
    private static final Consumer<PaymentScreen> assertSuccess = paymentScreen -> Assert.assertEquals(paymentScreen.buyStatus(),"PASS");
    private static final Consumer<PaymentScreen> assertFail = paymentScreen -> Assert.assertEquals(paymentScreen.buyStatus(),"FAIL");

    //test cases assert with data provider
    public static final Consumer<PaymentScreen> firstCaseDP = insertCcPass.andThen(buy);
    public static final Consumer<PaymentScreen> secondCaseDP = partialDiscount.andThen(insertCcPass).andThen(buy);
    public static final Consumer<PaymentScreen> thirdCaseDP = fullDiscount.andThen(buy);
    public static final Consumer<PaymentScreen> fourthCaseDP = partialDiscount.andThen(insertCcFail).andThen(buy);
    public static final Consumer<PaymentScreen> fifthCaseDP = insertCcFail.andThen(buy);
    public static final Consumer<PaymentScreen> sixthCaseDP = buy;

    //test cases assert with decorator
    public static final Consumer<PaymentScreen> firstCaseDC = insertCcPass.andThen(buy);
    public static final Consumer<PaymentScreen> secondCaseDC = partialDiscount.andThen(insertCcPass).andThen(buy);
    public static final Consumer<PaymentScreen> thirdCaseDC = fullDiscount.andThen(buy);
    public static final Consumer<PaymentScreen> fourthCaseDC = partialDiscount.andThen(insertCcFail).andThen(buy);
    public static final Consumer<PaymentScreen> fifthCaseDC = insertCcFail.andThen(buy);
    public static final Consumer<PaymentScreen> sixthCaseDC = buy;

}
