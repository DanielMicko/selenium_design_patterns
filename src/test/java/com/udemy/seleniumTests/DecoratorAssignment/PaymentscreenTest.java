package com.udemy.seleniumTests.DecoratorAssignment;

import com.udemy.seleniumDesign.DecoratorAssignment.PaymentScreen;
import com.udemy.seleniumTests.BaseTest;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.function.Consumer;

public class PaymentscreenTest extends BaseTest {


    private PaymentScreen paymentScreen;


    @BeforeTest
    public void setPage(){
        this.paymentScreen = new PaymentScreen(driver);
    }


    @Test(dataProvider = "getData1")
    public void paymentTestProviderAssertion(Consumer<PaymentScreen> paymentMethod, String status){
        this.paymentScreen.goTo();
        paymentMethod.accept(this.paymentScreen);
        Assert.assertEquals(paymentScreen.buyStatus(),status);
    }

    @Test(dataProvider = "getData2")
    public void paymentTestDecoratorAssertion(Consumer<PaymentScreen> paymentMethod){
        this.paymentScreen.goTo();
        paymentMethod.accept(this.paymentScreen);
    }

    @DataProvider
    public Object[][] getData1(){
        return new Object[][]{
                {AssignmentDecorators.firstCaseDP, "PASS"},
                {AssignmentDecorators.secondCaseDP, "PASS"},
                {AssignmentDecorators.thirdCaseDP, "PASS"},
                {AssignmentDecorators.fourthCaseDP, "FAIL"},
                {AssignmentDecorators.fifthCaseDP, "FAIL"},
                {AssignmentDecorators.sixthCaseDP, "FAIL"}
        };
    }

    @DataProvider
    public Object[] getData2(){
        return new Object[]{
                AssignmentDecorators.firstCaseDC,
                AssignmentDecorators.secondCaseDC,
                AssignmentDecorators.thirdCaseDC,
                AssignmentDecorators.fourthCaseDC,
                AssignmentDecorators.fifthCaseDC,
                AssignmentDecorators.sixthCaseDC
        };
    }
}
