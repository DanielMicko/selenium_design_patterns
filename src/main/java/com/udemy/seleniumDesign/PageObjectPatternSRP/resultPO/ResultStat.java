package com.udemy.seleniumDesign.PageObjectPatternSRP.resultPO;

import com.udemy.seleniumDesign.PageObjectPatternSRP.commonPO.abstractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ResultStat extends abstractComponent {

    @FindBy(id="result-stats")
    private WebElement stat;

    public ResultStat(WebDriver driver) {
        super(driver);
    }

    public String getStat(){
        return this.stat.getText();
    }

    @Override
    public boolean isDisplayed() {
        return this.wait.until(driver -> stat.isDisplayed());
    }
}
