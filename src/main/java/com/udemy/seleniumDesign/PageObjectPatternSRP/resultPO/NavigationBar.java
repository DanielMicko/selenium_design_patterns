package com.udemy.seleniumDesign.PageObjectPatternSRP.resultPO;

import com.udemy.seleniumDesign.PageObjectPatternSRP.commonPO.abstractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class NavigationBar extends abstractComponent {

    @FindBy(id="hdtb")
    private WebElement bar;

    @FindBy(linkText = "Videa")
    private WebElement videos;

    @FindBy(linkText = "Zprávy")
    private WebElement news;

    public NavigationBar(WebDriver driver) {
        super(driver);
    }

    public void goToVideos(){
        this.videos.click();
    }

    public void goToNews(){
        this.news.click();
    }

    @Override
    public boolean isDisplayed() {
        return this.wait.until(driver -> this.bar.isDisplayed());
    }
}
