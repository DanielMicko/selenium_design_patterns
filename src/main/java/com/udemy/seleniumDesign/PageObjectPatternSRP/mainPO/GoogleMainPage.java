package com.udemy.seleniumDesign.PageObjectPatternSRP.mainPO;

import com.udemy.seleniumDesign.PageObjectPatternSRP.commonPO.SearchSuggestion;
import com.udemy.seleniumDesign.PageObjectPatternSRP.commonPO.SearchWidget;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class GoogleMainPage {
    //toto je page object, nebude extendovat abstractComp
    //je to page object pouze pro google main page

    private WebDriver driver;
    private SearchWidget searchWidget;
    private SearchSuggestion searchSuggestion;

    //@FindBy(xpath="//span[text()='Souhlasím']")
    //private WebElement buttonAccept;

    public GoogleMainPage(final WebDriver driver){
        this.driver=driver;
        this.searchWidget = PageFactory.initElements(driver, SearchWidget.class);
        this.searchSuggestion = PageFactory.initElements(driver, SearchSuggestion.class);
    }

    public void goTo(){
        this.driver.get("https://www.google.com/");
    }

    public void acceptModal(){
        this.driver.switchTo().frame(0);
        //this.buttonAccept.click();
        driver.findElement(By.xpath("//span[text()='Souhlasím']")).click();
        this.driver.switchTo().defaultContent();
    }

    public SearchWidget getSearchWidget() {
        return searchWidget;
    }

    public SearchSuggestion getSearchSuggestion() {
        return searchSuggestion;
    }


}
