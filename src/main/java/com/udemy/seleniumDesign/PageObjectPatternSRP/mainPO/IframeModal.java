package com.udemy.seleniumDesign.PageObjectPatternSRP.mainPO;

import com.udemy.seleniumDesign.PageObjectPatternSRP.commonPO.abstractComponent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class IframeModal extends abstractComponent {

    WebDriver driver;

    @FindBy(linkText="Souhlasím")
    private WebElement buttonAccept;

    public IframeModal(WebDriver driver) {
        super(driver);
    }


    public void acceptModal(){
        this.driver.switchTo().frame(0);
        this.buttonAccept.click();
        this.driver.switchTo().defaultContent();
    }


    @Override
    public boolean isDisplayed() {
        return this.wait.until(driver -> this.buttonAccept.isDisplayed());
    }
}
