package com.udemy.seleniumDesign.StrategyPattern;

import java.util.Map;

public interface StrategyPatternPaymentOption {
    void enterPaymentInformation(Map<String, String> paymentDetails);
}
