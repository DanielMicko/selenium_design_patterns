package com.udemy.seleniumDesign.StrategyPattern.Main;

import com.udemy.seleniumDesign.StrategyPattern.StrategyPatternPaymentOption;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PaymentScreen {

    private WebDriver driver;
    private UserInformation userInformation;
    private Order order;
    private StrategyPatternPaymentOption paymentOption;


    public PaymentScreen(final WebDriver driver){
        this.driver = driver;
        this.userInformation = PageFactory.initElements(driver, UserInformation.class);
        this.order = PageFactory.initElements(driver, Order.class);
    }

    public void launchSite(){
        this.driver.get("https://vins-udemy.s3.amazonaws.com/ds/strategy.html");
    }

    public UserInformation getUserInformation(){
        return userInformation;
    }

    public Order getOrder(){
        return order;
    }

    public void setPaymentOption(StrategyPatternPaymentOption paymentOption){
        this.paymentOption = paymentOption;
        PageFactory.initElements(driver, this.paymentOption);
    }

    public StrategyPatternPaymentOption getPaymentOption(){
        return paymentOption;
    }

}
