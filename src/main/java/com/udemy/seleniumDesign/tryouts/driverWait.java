package com.udemy.seleniumDesign.tryouts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class driverWait {
    WebDriver driver;

    public driverWait(WebDriver driver){
        this.driver = driver;
    }

    public void driverWaitVisible(String xpath){
        new WebDriverWait(driver,7)
                .until(ExpectedConditions
                        .visibilityOfElementLocated(By.xpath(xpath)));
    }
}
