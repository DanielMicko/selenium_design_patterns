package com.udemy.seleniumDesign.tryouts;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class loginPageNew {

        WebDriver driver;

        public loginPageNew(WebDriver driver){
            this.driver = driver;
        }

        @FindBy(id = "Username")
        //@CacheLookup   ---   Zapsání proměnné do cache, aby nemusel každý test prozkoumávat classu. Zvýšení rychlosti, ale pozor na výkon hw
        WebElement userName;

        @FindBy(id = "Password")
        WebElement password;

        @FindBy(xpath = "//button[@value='login']")
        WebElement button;

        public void login_nebanka(String us, String pas){
            userName.sendKeys(us);
            password.sendKeys(pas);
            button.click();
        }
}
