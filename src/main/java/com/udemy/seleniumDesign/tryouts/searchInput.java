package com.udemy.seleniumDesign.tryouts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class searchInput {
    WebDriver driver;

    public searchInput(WebDriver driver){
        this.driver = driver;
    }

    public void searchGoogle(String xpath,String text){
        if(driver.findElement(By.xpath(xpath)).isDisplayed()) {
            WebElement we = driver.findElement(By.xpath(xpath));
            new Actions(driver).moveToElement(we).click().sendKeys(text).build().perform();
        }
    }

}

