package com.udemy.seleniumDesign.tryouts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class selectFromDropdown {

    WebDriver driver;

    public selectFromDropdown(WebDriver driver){
        this.driver = driver;
    }

    public void selectListElement(String xpath, int index){
        if(driver.findElement(By.xpath(xpath)).isDisplayed()){
            WebElement dropdown = driver.findElement(By.xpath(xpath));
            List<WebElement> options = dropdown.findElements(By.tagName("li"));

            WebElement select = options.get(index);
            select.click();
        }
    }
}
