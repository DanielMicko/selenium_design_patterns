package com.udemy.seleniumDesign.tryouts;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


/*

    This class will store all the locators on given pages

 */

public class loginPage {

    WebDriver driver;

    By username = By.id("Username");
    By password = By.id("Password");
    By loginButton = By.xpath("//button[@value='login']");


    public loginPage(WebDriver driver) {
        this.driver = driver;
    }

    public void typeUserName(String email){
        driver.findElement(username).sendKeys(email);
    }

    public void typePassword(String pass){
        driver.findElement(password).sendKeys(pass);
    }

    public void clickOnLogin(){
        driver.findElement(loginButton).click();
    }

}
