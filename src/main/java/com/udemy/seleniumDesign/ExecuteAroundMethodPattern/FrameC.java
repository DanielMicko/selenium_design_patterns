package com.udemy.seleniumDesign.ExecuteAroundMethodPattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FrameC {

    @FindBy(name = "fn")
    private WebElement firstName;

    @FindBy(name = "ln")
    private WebElement lastName;

    @FindBy(name = "addr")
    private WebElement address;

    @FindBy(id = "area")
    private WebElement message;


    public void inputFn(String firstName){
        this.firstName.sendKeys(firstName);
    }

    public void inputLn(String lastName){
        this.lastName.sendKeys(lastName);
    }

    public void inputAdd(String address){
        this.address.sendKeys(address);
    }

    public void inputMess(String message){
        this.message.sendKeys(message);
    }
}
