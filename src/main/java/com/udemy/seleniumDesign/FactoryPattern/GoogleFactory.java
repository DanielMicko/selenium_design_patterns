package com.udemy.seleniumDesign.FactoryPattern;

import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class GoogleFactory {

    private static final Function<WebDriver, GoogleSearchPage> ENG = driver -> new GoogleEnglish(driver);
    private static final Function<WebDriver, GoogleSearchPage> FR = driver -> new GoogleFrench(driver);
    private static final Function<WebDriver, GoogleSearchPage> SA = driver -> new GoogleArabic(driver);
    private static final Map<String, Function<WebDriver, GoogleSearchPage>> MAP = new HashMap<>();

    static {
        MAP.put("ENG", ENG);
        MAP.put("FR", FR);
        MAP.put("SA", SA);
    }

    //constructor factory metody
    public static GoogleSearchPage get(String language, WebDriver driver){
        return MAP.get(language).apply(driver);
    }


}