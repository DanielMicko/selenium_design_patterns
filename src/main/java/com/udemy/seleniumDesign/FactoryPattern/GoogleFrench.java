package com.udemy.seleniumDesign.FactoryPattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

class GoogleFrench extends GoogleEnglish {

    @FindBy(id = "regionoFR")
    private WebElement selectFrance;

    @FindBy(linkText = "Français")
    private WebElement frenchLngButton;


    public GoogleFrench(WebDriver driver) {
        super(driver);
    }


    @Override
    public void launchSite() {
        this.driver.get("https://www.google.fr/");
    }

    @Override
    public void selectLanguage() {
        this.selectFrance.click();
    }

    @Override
    public void search(String keyword) {
        this.frenchLngButton.click();
        this.wait.until(driver -> searchBox.isDisplayed());
        super.search(keyword);
    }

}
