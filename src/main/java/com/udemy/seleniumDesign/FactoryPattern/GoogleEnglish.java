package com.udemy.seleniumDesign.FactoryPattern;

import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

class GoogleEnglish extends GoogleSearchPage {

    protected WebDriver driver;
    protected WebDriverWait wait;

    @FindBy(className = "fkimef I47yTd k8Lt0")
    private WebElement iframe;

    @FindBy(id = "Mses6b")
    private WebElement settings;

    @FindBy(className = "EzVRq")
    private WebElement searchSettings;

    @FindBy(className = "UyLz9e")
    private WebElement showAllButton;

    @FindBy(id = "regionoGB")
    private WebElement englishLngButton;

    @FindBy(xpath = "//*[@id='form-buttons']/div[1]")
    private WebElement saveSettingsButton;

    @FindBy(name = "q")
    protected WebElement searchBox;

    @FindBy(name = "btnK")
    private WebElement searchButton;

    @FindBy(className="g")
    private List<WebElement> results;


    public GoogleEnglish(final WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver,this);
    }


    @Override
    public void launchSite() {
        this.driver.get("https://www.google.com/");
    }

    @Override
    public void acceptCookies() {
        try {
            this.driver.switchTo().frame(0);
            driver.findElement(By.xpath("//span[text()='Souhlasím']")).click();
            this.driver.switchTo().defaultContent();
        }
        catch(NoSuchFrameException e){
            System.out.println("no iframe present");
        }
    }

    @Override
    public void settings() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        this.settings.click();
        //this.wait.until(driver -> searchSettings.isDisplayed());
        js.executeScript("document.getElementsByClassName('EzVRq')[1].click()");
        //this.searchSettings.click();
        this.wait.until(driver -> showAllButton.isDisplayed());
        this.showAllButton.click();
    }

    @Override
    public void selectLanguage() {
        this.englishLngButton.click();
    }

    @Override
    public void saveLanguage() {
        this.saveSettingsButton.click();
        this.wait.until(ExpectedConditions.alertIsPresent());
        this.driver.switchTo().alert().accept();
        this.wait.until(driver -> searchBox.isDisplayed());
    }

    @Override
    public void search(String keyword) {
        for (char ch: keyword.toCharArray()){
            Uninterruptibles.sleepUninterruptibly(10, TimeUnit.MILLISECONDS);
            this.searchBox.sendKeys(ch + "");
        }
        this.wait.until(driver -> searchButton.isDisplayed());
        this.searchButton.click();
    }

    @Override
    public int getResultsCount() {
        this.wait.until(driver -> this.results.size()>1);
        return this.results.size();
    }
}
