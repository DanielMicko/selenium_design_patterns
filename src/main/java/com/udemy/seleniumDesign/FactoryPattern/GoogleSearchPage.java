package com.udemy.seleniumDesign.FactoryPattern;

public abstract class GoogleSearchPage {

    public abstract void launchSite();
    public abstract void acceptCookies();
    public abstract void settings();
    public abstract void selectLanguage();
    public abstract void saveLanguage();
    public abstract void search(String keyword);
    public abstract int getResultsCount();

}
