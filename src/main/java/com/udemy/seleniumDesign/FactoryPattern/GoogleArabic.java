package com.udemy.seleniumDesign.FactoryPattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

class GoogleArabic extends GoogleEnglish{

    @FindBy (id = "regionoAE")
    private WebElement selectArab;

    @FindBy(xpath = "//a[@dir='rtl']")
    private WebElement arabLngButton;

    @FindBy(css = "span.hOoLGe")
    private WebElement keyboardButton;

    @FindBy(id = "kbd")
    private WebElement keyboard;


    public GoogleArabic(WebDriver driver) {
        super(driver);
    }


    @Override
    public void launchSite(){
        this.driver.get("https://www.google.com.sa");
    }

    @Override
    public void selectLanguage() {
        this.selectArab.click();
    }

    @Override
    public void search(String keyword){
        this.arabLngButton.click();
        this.wait.until(driver1 -> keyboardButton.isDisplayed());
        this.keyboardButton.click();
        this.wait.until(driver1 -> keyboard.isDisplayed());
        super.search(keyword);
    }

}
