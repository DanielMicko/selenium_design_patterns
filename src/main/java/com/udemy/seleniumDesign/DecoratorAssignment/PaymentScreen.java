package com.udemy.seleniumDesign.DecoratorAssignment;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PaymentScreen {

    private WebDriver driver;

    @FindBy(id = "coupon")
    private WebElement couponInput;

    @FindBy(id = "couponbtn")
    private WebElement couponBtn;

    @FindBy(id = "cc")
    private WebElement cc;

    @FindBy(id = "year")
    private WebElement year;

    @FindBy(id = "cvv")
    private WebElement cvv;

    @FindBy(id = "price")
    private WebElement price;

    @FindBy(id = "buy")
    private WebElement buyBtn;

    @FindBy(id = "status")
    private WebElement status;

    /*Purchase should be successful using valid CC
    Purchase should be successful using discounted promocode + valid CC
    Purchase should be successful using FREE promocode
    Purchase should fail using discounted promocode + invalid CC
    Purchase should fail using invalid CC
    Purchase should fail without any payment info*/

    public PaymentScreen (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void goTo(){
        this.driver.get("https://vins-udemy.s3.amazonaws.com/java/html/java8-payment-screen.html");
    }

    public void insertCreditCard (String cc, String year, String cvv){
        this.cc.sendKeys(cc);
        this.year.sendKeys(year);
        this.cvv.sendKeys(cvv);
    }

    public void insertCode (String code){
        this.couponInput.sendKeys(code);
        this.couponBtn.click();
    }

    public void buy(){
        this.buyBtn.click();
    }

    public String buyStatus(){
        return this.status.getText().trim();
    }

}
