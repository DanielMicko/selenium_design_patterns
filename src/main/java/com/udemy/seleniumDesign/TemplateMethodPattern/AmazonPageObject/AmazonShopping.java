package com.udemy.seleniumDesign.TemplateMethodPattern.AmazonPageObject;

import com.udemy.seleniumDesign.TemplateMethodPattern.ShoppingTemplate;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

//Takto by měly vypadat správně napsané page objecty. Vše rozdělit podle SRP

public class AmazonShopping extends ShoppingTemplate {

    private WebDriver driver;
    private String product;

    private AmazonSearchPage amazonSearchPage;
    private AmazonResultPage amazonResultPage;
    private AmazonProductPage amazonProductPage;

    public AmazonShopping(WebDriver driver, String product){
        this.driver = driver;
        this.product = product;
        this.amazonSearchPage = PageFactory.initElements(driver,AmazonSearchPage.class);
        this.amazonResultPage = PageFactory.initElements(driver,AmazonResultPage.class);
        this.amazonProductPage = PageFactory.initElements(driver,AmazonProductPage.class);
    }

    @Override
    public void launchSite() {
        this.amazonSearchPage.goTo();
    }

    @Override
    public void searchForProduct() {
        this.amazonSearchPage.search(this.product);
    }

    @Override
    public void selectProduct() {
        this.amazonResultPage.select();
    }

    @Override
    public void buy() {
        this.amazonProductPage.price();
    }
}
