package com.udemy.seleniumDesign.TemplateMethodPattern.AmazonPageObject;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AmazonProductPage {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(id="priceblock_ourprice")
    private WebElement price;

    public AmazonProductPage (WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 30);
        PageFactory.initElements(driver,this);
    }

    public void price() {
        try {
            this.wait.until(d -> this.price.isDisplayed());
            System.out.println(this.price.getText());
        }
        catch (NoSuchElementException e){
            System.out.println("Product had no price.");
        }
    }
}
