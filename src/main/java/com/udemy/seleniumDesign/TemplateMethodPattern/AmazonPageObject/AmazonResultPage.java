package com.udemy.seleniumDesign.TemplateMethodPattern.AmazonPageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AmazonResultPage {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//span[@class='a-size-medium a-color-base a-text-normal']")
    private WebElement item;

    public AmazonResultPage(WebDriver driver){
        this.driver = driver;
        this.wait = new WebDriverWait(driver,30);
        PageFactory.initElements(driver,this);
    }

    public void select() {
        this.wait.until(d -> this.item.isDisplayed());
        this.item.click();
    }
}
