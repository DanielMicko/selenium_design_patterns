package com.udemy.seleniumDesign.TemplateMethodPattern;

public abstract class ShoppingTemplate {
    //kroky každé classy
    public abstract void launchSite();
    public abstract void searchForProduct();
    public abstract void selectProduct();
    public abstract void buy();

    //algoritmus každé classy
    public void shop(){
        launchSite();
        searchForProduct();
        selectProduct();
        buy();
    }

}
