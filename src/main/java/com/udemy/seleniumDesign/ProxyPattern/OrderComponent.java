package com.udemy.seleniumDesign.ProxyPattern;

public interface OrderComponent {
    String placeOrder();

}
