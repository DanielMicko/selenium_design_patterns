package com.udemy.seleniumDesign.ProxyPattern.Main;

import com.udemy.seleniumDesign.ProxyPattern.OrderComponent;
import com.udemy.seleniumDesign.ProxyPattern.OrderComponentProxy;
import com.udemy.seleniumDesign.ProxyPattern.StrategyPatternPaymentOption;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PaymentScreen {

    private WebDriver driver;
    private UserInformation userInformation;
    private OrderComponent orderComponent;
    private StrategyPatternPaymentOption paymentOption;


    public PaymentScreen(final WebDriver driver){
        this.driver = driver;
        this.userInformation = PageFactory.initElements(driver, UserInformation.class);
        this.orderComponent = new OrderComponentProxy(driver);
    }

    public void launchSite(){
        this.driver.get("https://vins-udemy.s3.amazonaws.com/ds/strategy.html");
    }

    public UserInformation getUserInformation(){
        return userInformation;
    }

    public OrderComponent getOrder(){
        return orderComponent;
    }

    public void setPaymentOption(StrategyPatternPaymentOption paymentOption){
        this.paymentOption = paymentOption;
        PageFactory.initElements(driver, this.paymentOption);
    }

    public StrategyPatternPaymentOption getPaymentOption(){
        return paymentOption;
    }

}
