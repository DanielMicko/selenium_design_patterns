package com.udemy.seleniumDesign.ProxyPattern;

import com.udemy.seleniumDesign.ProxyPattern.PaymentMethod.CreditCard;
import com.udemy.seleniumDesign.ProxyPattern.PaymentMethod.NetBanking;
import com.udemy.seleniumDesign.ProxyPattern.PaymentMethod.PayPal;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class FactoryPatternPaymentOption {

    private static final Supplier<StrategyPatternPaymentOption> CC = () -> new CreditCard();
    private static final Supplier<StrategyPatternPaymentOption> NB = () -> new NetBanking();
    private static final Supplier<StrategyPatternPaymentOption> PP = () -> new PayPal();

    private static final Map<String, Supplier<StrategyPatternPaymentOption>> MAP = new HashMap<>();

    static {
        MAP.put("CC",CC);
        MAP.put("NB",NB);
        MAP.put("PP",PP);
    }

    public static StrategyPatternPaymentOption get(String option){
        return MAP.get(option).get(); //get method belongs to supplier, apply method belongs to function
    }

}
