package com.udemy.seleniumDesign.ProxyPattern;

import java.util.Map;

public interface StrategyPatternPaymentOption {
    void enterPaymentInformation(Map<String, String> paymentDetails);
}
