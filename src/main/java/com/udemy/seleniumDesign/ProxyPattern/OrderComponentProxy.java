package com.udemy.seleniumDesign.ProxyPattern;

import org.openqa.selenium.WebDriver;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class OrderComponentProxy implements OrderComponent{

    private static final List<String> EXCLUDED = Arrays.asList("PROD","STAGING"); //tyto weby odstraňujeme z workflow - tedy nebude se testovat na production a staging
    private OrderComponent orderComponent;

    public OrderComponentProxy(WebDriver driver){
        String currentEnv = System.getProperty("env"); // DEV / QA / PROD / STAGING
        if(!EXCLUDED.contains(currentEnv)){ //pokud EXCLUDED neobsahuje current environment
            this.orderComponent = new OrderComponentReal(driver); //vytvoří se nová instance OrderComponentReal classy
        }
    }

    @Override
    public String placeOrder() {
        if(Objects.nonNull(this.orderComponent)){ //Pokud je vytvořená instance classy
            return this.orderComponent.placeOrder(); //zavolá se metoda placeOrder
        } else {
            return "SKIPPED";
        }
    }
}
