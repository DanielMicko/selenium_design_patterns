package com.udemy.seleniumDesign.ProxyPattern.PaymentMethod;

import com.udemy.seleniumDesign.ProxyPattern.StrategyPatternPaymentOption;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.Map;

public class NetBanking implements StrategyPatternPaymentOption {


    @FindBy(id = "bank")
    private WebElement bank;

    @FindBy(id = "acc_number")
    private WebElement accNum;

    @FindBy(id = "pin")
    private WebElement pin;


    @Override
    public void enterPaymentInformation(Map<String, String> paymentDetails) {
        Select bankDropdown = new Select(bank);
        bankDropdown.selectByValue(paymentDetails.get("bank"));
        this.accNum.sendKeys(paymentDetails.get("accNum"));
        this.pin.sendKeys(paymentDetails.get("pin"));
    }
}
