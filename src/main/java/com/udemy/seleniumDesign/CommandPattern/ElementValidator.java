package com.udemy.seleniumDesign.CommandPattern;

public abstract class ElementValidator {
    public abstract boolean validate();

}
