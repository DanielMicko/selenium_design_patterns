package com.udemy.seleniumDesign.CommandPattern;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import java.util.Arrays;
import java.util.List;

public class HomePage {

    private final WebDriver driver;

    //trigger notification button
    @FindBy(css = "div.button-box button.btn-info")
    private WebElement infoButton;

    @FindBy(css = "div.button-box button.btn-warning")
    private WebElement warningButton;

    @FindBy(css = "div.button-box button.btn-success")
    private WebElement successButton;

    @FindBy(css = "div.button-box button.btn-danger")
    private WebElement dangerButton;

    //notifications
    @FindBy(className = "jq-toast-wrap top-right")
    private WebElement toastrBox;

    @FindBy(css = "div.jq-icon-info")
    private WebElement infoToastr;

    @FindBy(css = "div.jq-icon-warning")
    private WebElement warningToastr;

    @FindBy(css = "div.jq-icon-success")
    private WebElement successToastr;

    @FindBy(css = "div.jq-icon-error")
    private WebElement dangerToastr;

    //dismissal alert
    @FindBy(xpath = "//div[@class='col-lg-4 col-md-12'][2]/div[@class='alert alert-success']")
    private WebElement successAlert;

    @FindBy(xpath = "//div[@class='col-lg-4 col-md-12'][2]/div[@class='alert alert-danger']")
    private WebElement dangerAlert;

    @FindBy(xpath = "//div[@class='col-lg-4 col-md-12'][2]/div[@class='alert alert-warning']")
    private WebElement warningAlert;

    @FindBy(xpath = "//div[@class='col-lg-4 col-md-12'][2]/div[@class='alert alert-info']")
    private WebElement infoAlert;

    //buttons
    @FindBy(xpath = "//div[@class='col-lg-4 col-md-12'][2]/div[@class='alert alert-success']/button")
    private WebElement successBtn;

    @FindBy(xpath = "//div[@class='col-lg-4 col-md-12'][2]/div[@class='alert alert-danger']/button")
    private WebElement dangerBtn;

    @FindBy(xpath = "//div[@class='col-lg-4 col-md-12'][2]/div[@class='alert alert-warning']/button")
    private WebElement warningBtn;

    @FindBy(xpath = "//div[@class='col-lg-4 col-md-12'][2]/div[@class='alert alert-info']/button")
    private WebElement infoBtn;


    public HomePage(final WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public void launchSite(){
        this.driver.get("http://wrappixel.com/demos/admin-templates/admin-pro/main/ui-notification.html");
    }

    public List<ElementValidator> getElementValidators(){

        return Arrays.asList(
                //click and validate notification
                new NotificationValidator(infoButton, infoToastr),
                new NotificationValidator(successButton, successToastr),
                new NotificationValidator(warningButton, warningToastr),
                new NotificationValidator(dangerButton, dangerToastr),
                //validate dismiss notification
                new DismissalAlertValidator(successAlert,successBtn),
                new DismissalAlertValidator(dangerAlert,dangerBtn),
                new DismissalAlertValidator(warningAlert,warningBtn),
                new DismissalAlertValidator(infoAlert,infoBtn)
        );

    }


}
