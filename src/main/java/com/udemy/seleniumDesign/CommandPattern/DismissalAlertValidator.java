package com.udemy.seleniumDesign.CommandPattern;


import com.google.common.util.concurrent.Uninterruptibles;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.concurrent.TimeUnit;

public class DismissalAlertValidator extends ElementValidator{

    private final WebElement dismissalAlert;
    private final WebElement button;

    public DismissalAlertValidator(final WebElement element, final WebElement btn){
        this.dismissalAlert = element;
        this.button = btn;
    }

    @Override
    public boolean validate() {
        boolean result1 = this.dismissalAlert.isDisplayed();//true
        System.out.println(result1);
        this.button.click();
        Uninterruptibles.sleepUninterruptibly(2, TimeUnit.SECONDS);
        boolean result2 = this.dismissalAlert.isDisplayed();//false
        System.out.println(result2);
        return result1 && (!result2);
    }

}
